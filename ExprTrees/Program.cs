﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExprTrees
{
    class Program
    {
        static void Main(string[] args)
        {
            /* *
             *  Expression API is much faster then Reflection API
             *  but u must cache delegates for this performance 
             * */


            //============== Adding item to collection start =============

            var obj = new Obje();
            var pe = Expression.Parameter(typeof(Obje), "x"); // x 
            var prop = Expression.Property(pe, "Items"); // x.Items
            var call = Expression.Call(prop, typeof(ICollection<string>).GetMethod("Add"), Expression.Constant("111")); // x.Items.Add("111")
            var lambda = Expression.Lambda(call, pe); // (x) => x.Items.Add("111")

            var compiled = lambda.Compile(); // Compiling is slow , u must cache delegate
            compiled.DynamicInvoke(obj);

            //============= Adding item to collection end ===============

            //============== Adding item to collection parametrized start =============

            var p1 = Expression.Parameter(typeof(Obje), "x"); // x
            var p2 = Expression.Parameter(typeof(string), "p"); // p
            var prop1 = Expression.Property(p1, "Items"); // x.Items
            var call2 = Expression.Call(prop1, typeof(ICollection<string>).GetMethod("Add"), p2); // x.Items.Add(p)
            var lambda1 = Expression.Lambda(call2, p1, p2); // (x,p) => x.Items.Add(p)

            var compiled1 = lambda1.Compile();
            compiled1.DynamicInvoke(obj, "22");

            //============== Adding item to collection parametrized end =============

            //============= Assigning property start ==============

            var param = Expression.Parameter(typeof(Obje), "x"); // x
            var property = Expression.Property(param, "Name"); // x.Name
            var assign = Expression.Assign(property, Expression.Constant("Jimm")); // x.Name == "Jimm"
            var lambda2 = Expression.Lambda(assign, param); // (x) => x.Name == "Jimm"

            var compiled2 = lambda2.Compile();
            compiled2.DynamicInvoke(obj);

            //============= Assigning property end ==============

            //============= Getting value start =============

            var par = Expression.Parameter(typeof(Obje), "x"); // x
            var propert = Expression.Property(par, "Name"); // x.Name
            var lambda3 = Expression.Lambda(propert, par); // x => x.Name
            var compiled3 = lambda3.Compile();
            var value = compiled3.DynamicInvoke(obj) as string;

            //============ Getting value end ============

            var param1 = Expression.Parameter(typeof(int), "x"); // x
            var greaterThen = Expression.GreaterThan(param1, Expression.Constant(10)); // x > 10
            var andLessThen = Expression.And(greaterThen, Expression.LessThan(param1, Expression.Constant(20))); // x > 10 && x < 20
            var lambda4 = Expression.Lambda(andLessThen, param1); // x => x > 10 && x < 20

            var compiled4 = lambda4.Compile();

            var compare = (bool)compiled4.DynamicInvoke(30);

            var isDefault = @obj.IsPropertyDefault("SecondName");
            var isDefault2 = obj.IsPropertyDefault("Name");
            var isDefault3 = obj.IsPropertyDefault("Items");
            var anyName = obj.AnyCustomNamesInObjects("Jimm");

            Debug.Assert(!isDefault, "Is default not working");
            Debug.Assert(compare == false);
            Debug.Assert(value != null && value == "Jimm");
            Debug.Assert(obj.Items.Count > 0);
            Debug.Assert(obj.Name == "Jimm");
        }
    }

    public class Obje : IDisposable
    {
        public Obje()
        {
            Items = new Collection<string>();
        }

        public ICollection<string> Items { get; set; }

        public ICollection<Obje> Objects
        {
            get
            {
                return new Collection<Obje>() { 
                    new Obje {
                        Name = "Jimm"
                    },
                    new Obje {
                        Name = "Sam"
                    }
                };
            }
        }

        public string Name { get; set; }

        public string SecondName { get; set; }

        public void Dispose()
        {
            Debug.WriteLine("Obj is disposed");
        }

        public bool AnyCustomNamesInObjects(string name)
        {
            var parameter = Expression.Parameter(typeof(Obje), "x");
            var property = Expression.Property(parameter, "Objects");

            var innerExpression = Expression.Parameter(typeof(Obje), "y");
            var nameProperty = Expression.Property(innerExpression, "Name");
            var equalExpression = Expression.Equal(nameProperty, Expression.Constant(name));
            var innerLambda = Expression.Lambda(equalExpression, innerExpression);

            var call = Expression.Call(typeof(Enumerable), "Any", new Type[] { typeof(Obje) }, property, innerLambda);
            var lambda = Expression.Lambda(call, parameter);

            var compiled = lambda.Compile();

            return (bool)compiled.DynamicInvoke(this);
        }

        ~Obje()
        {
            Debug.WriteLine("Obj is destroyed with name: " + Name);
        }
    }

    public static class ObjectExtensions
    {
        private static IDictionary<string, Delegate> isNullOrDefaultLambdasCache = new Dictionary<string, Delegate>();

        /// <summary>
        /// Checking for default value of property.
        /// <remarks>
        /// Like <c>x.Property == default(PropertyType)</c>
        /// </remarks>
        /// </summary>
        /// <param name="object">object which extends</param>
        /// <param name="propertyName">name of property in object</param>
        /// <returns>true if default, and false if not</returns>
        public static bool IsPropertyDefault(this object @object, string propertyName)
        {
            var type = @object.GetType();
            var key = string.Format("{0}-{1}", type.Name, propertyName);

            if (isNullOrDefaultLambdasCache.ContainsKey(key))
                return (bool)isNullOrDefaultLambdasCache[key].DynamicInvoke(@object);

            var properties = TypeDescriptor.GetProperties(type)
                .OfType<PropertyDescriptor>();

            foreach (var item in properties)
            {
                if (item.Name != propertyName)
                    continue;

                var parameter = Expression.Parameter(type, "x"); // x
                var property = Expression.Property(parameter, propertyName); // x.Property
                var @default = Expression.Default(item.PropertyType); // default(T)
                var isDefault = Expression.Equal(property, @default); // x.Property == default(T)
                var lambda = Expression.Lambda(isDefault, parameter); // x => x.Property == default(T)

                var compiledLambda = lambda.Compile();
                isNullOrDefaultLambdasCache.Add(key, compiledLambda);

                return (bool)compiledLambda.DynamicInvoke(@object);
            }

            return false;
        }
    }
}
